<!--
SPDX-FileCopyrightText: 2023 Mathis Brüchert <mbb@kaidan.im>

SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
-->

# <img src="https://invent.kde.org/mbruchert/rattlesnake/-/raw/master/logo.png"  height="64" >  Rattlesnake


## A metronome app for musicians on mobile and desktop devices

![](https://i.imgur.com/lon9MC0.png)

### Feature
- Tap to the song you want to play and Rattlesnake will automaticly figure out the right BPM
- Build your beat from multiple sounds and adjust the volume of individual beats
